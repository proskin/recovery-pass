<?php declare(strict_types=1);

namespace App\CoreModule\Presenter;

use Nette\Application\UI\Presenter;

abstract class BasePresenter extends Presenter
{

}