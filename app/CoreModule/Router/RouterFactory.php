<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;

final class RouterFactory
{
	use Nette\StaticClass;

	public static function createRouter(): RouteList
	{
		$router = new Routers\RouteList;

		$clientRouter = new Routers\RouteList('Client');
		$clientRouter[] = new Routers\Route('/obnova-hesla', 'RecoveryPass:default');

		$router[] = $clientRouter;

		return $router;

	}
}
