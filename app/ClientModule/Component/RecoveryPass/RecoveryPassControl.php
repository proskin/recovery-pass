<?php declare(strict_types=1);

namespace App\ClientModule\Component\RecoveryPass;

use App\CoreModule\Factory\FormFactory;
use App\ClientModule\Service\RecoveryPassService;
use Nette\Application\UI\Control;
use Nette\Forms\Form;

final class RecoveryPassControl extends Control
{

	/** @var FormFactory */
	private $formFactory;

	/** @var RecoveryPassService */
	private $recoveryPassService;

	/** @var callable */
	public $onSuccessFormSend;

	public function __construct(
		RecoveryPassService $recoveryPassService,
		FormFactory $formFactory
	) {
		$this->recoveryPassService = $recoveryPassService;
		$this->formFactory = $formFactory;
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . '/../templates/Component/recoveryPass.latte');

		$this->template->render();
	}

	public function createComponentRecoveryPss(): Form
	{
		$form = $this->formFactory->create();

		$form->addEmail('email', 'E-mail')->setRequired(TRUE);

		$form->addSubmit('send', 'Obnov heslo');

		$form->onSuccess[] = [$this, 'recoveryPassCallback'];

		return $form;
	}

	public function recoveryPassCallback(Form $form): void
	{
		try {
			//@TODO send recovery email.. etc..	and change catch exceptions

		} catch (\SomeExpectedException $e) {
			$form->addError('User dont exist.. or something like this');

		} catch (\Exception $e) {
			$form->addError('Unexpected error');
			// @TODO log error
		}

		if ($form->hasErrors() === FALSE) {
			$this->onSuccessFormSend($form->value->email);
		}
	}

}