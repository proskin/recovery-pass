<?php declare(strict_types=1);

namespace App\ClientModule\Component\RecoveryPass;

interface IRecoveryPassControl
{
	/**
	 * @return \App\ClientModule\Component\IRecoveryPass
	 */
	function create();
}