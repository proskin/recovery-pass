<?php declare(strict_types=1);

namespace App\ClientModule\Presenters;

use App\CoreModule\Presenter\BasePresenter;
use App\ClientModule\Component\RecoveryPass\RecoveryPassControl;
use Nette\Application\UI\Control;

final class RecoveryPassPresenter extends BasePresenter
{

	/** @var RecoveryPassControl */
	private $recoveryPassControl;

	public function createComponentApprovalsForm(): Control
	{
		$control = $this->recoveryPassControl->create();

		$control->onSuccessFormSend[] = function () {

			// @TODO translator
			$this->flashMessage('Vaše heslo bylo obnoveno');

			$this->redirect('this');
		};

		return $control;
	}

}